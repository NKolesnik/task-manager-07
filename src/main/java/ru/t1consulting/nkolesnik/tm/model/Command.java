package ru.t1consulting.nkolesnik.tm.model;

import ru.t1consulting.nkolesnik.tm.constant.ArgumentConst;
import ru.t1consulting.nkolesnik.tm.constant.TerminalConst;

public class Command {

    public static Command ABOUT = new Command(TerminalConst.ABOUT, ArgumentConst.ABOUT, "Show developer info.");

    public static Command HELP = new Command(TerminalConst.HELP, ArgumentConst.HELP, "Show terminal commands.");

    public static Command VERSION = new Command(TerminalConst.VERSION, ArgumentConst.VERSION, "Show application version.");

    public static Command INFO = new Command(TerminalConst.INFO, ArgumentConst.INFO, "Show system info.");

    public static Command EXIT = new Command(TerminalConst.EXIT, "Close application.\n");

    private String name = "";

    private String argument = "";

    private String description = "";

    public Command() {
    }

    public Command(String name) {
        this.name = name;
    }

    public Command(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        String result = "";
        if(name != null && !name.isEmpty()){
            result+=name + " : ";
        }
        if(argument != null && !argument.isEmpty()){
            result+=argument + " : ";
        }
        if(description != null && !description.isEmpty()){
            result+=description;
        }
        return result;
    }

}
